package repositorio;

import java.util.ArrayList;
import java.util.List;

import model.Fornecedor;
import repositorio.erros.FornecedorException;
import iRepositorio.IRepositorioFornecedor;

public class FornecedorRepositorio implements IRepositorioFornecedor {
	
	private static FornecedorRepositorio instance;
	private List<Fornecedor> fornecedores = new ArrayList<>();
	
	public static FornecedorRepositorio getInstance(){
		if(instance == null){
			instance = new FornecedorRepositorio();
		}
		return instance;
	}

	public void cadastrarFornecedor(Fornecedor fornecedor) throws FornecedorException {
		fornecedores.add(fornecedor);
	}

	public void removerFornecedor(String cnpj) throws FornecedorException {
		for (Fornecedor f : fornecedores) {
			if(f.getCnpj().equals(cnpj)) {
				fornecedores.remove(f);
			}
		}
	}

	public Fornecedor consultarFornecedor(String cnpj) throws FornecedorException {
		for (int i = 0; i < fornecedores.size(); i++) {
			if (fornecedores.get(i).getCnpj().equals(cnpj)) {
				return fornecedores.get(i);
			}
		}
		return null;
	}

	public List<Fornecedor> listarFornecedor() {
		return this.fornecedores;
	}
	
}
