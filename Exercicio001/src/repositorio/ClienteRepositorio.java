package repositorio;

import java.util.ArrayList;
import java.util.List;

import model.Cliente;
import repositorio.erros.ClienteException;
import iRepositorio.IRepositorioCliente;

public class ClienteRepositorio implements IRepositorioCliente {
	private static ClienteRepositorio instance;
	private List<Cliente> clientes = new ArrayList<>();
	
	public static ClienteRepositorio getInstance(){
		if(instance == null){
			instance = new ClienteRepositorio();
		}
		return instance;
	}

	public void cadastrar(Cliente cliente) throws ClienteException {
		cliente.setId(clientes.size()+1);
		clientes.add(cliente);
	}

	public void remover(Cliente cliente) throws ClienteException  {
		clientes.remove(cliente);
	}

	public Cliente consultar(String cpf) throws ClienteException  {		
		for (Cliente c: clientes) {
			if (c.getCpf() == cpf) {
				return c;
			}
		}
		return null;
	}

	public List<Cliente> listar() {
		return this.clientes;
	}
	
}
