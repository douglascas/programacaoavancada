package repositorio;

import java.util.ArrayList;
import java.util.List;

import model.Produto;
import repositorio.erros.ProdutoException;
import iRepositorio.IRepositorioProduto;

public class ProdutoRepositorio implements IRepositorioProduto {
	private static ProdutoRepositorio instance;
	private List<Produto> produtos = new ArrayList<>();
	
	public static ProdutoRepositorio getInstance(){
		if(instance == null){
			instance = new ProdutoRepositorio();
		}
		return instance;
	}

	public void cadastrar(Produto produto) throws ProdutoException {
		produtos.add(produto);
	}

	public void remover(Produto produto) throws ProdutoException {
		produtos.remove(produto);
	}

	public Produto consultar(int id) throws ProdutoException {
		for (Produto p : produtos) {
			if (p.getCodigo() == id) {
				return p;
			}
		}
		return null;
	}

	public List<Produto> listar() {
		return this.produtos;
	}
	
}
