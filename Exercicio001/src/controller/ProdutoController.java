package controller;

import java.util.List;

import controller.api.IProdutosController;
import iRepositorio.IRepositorioProduto;
import model.Produto;
import repositorio.erros.ProdutoException;

public class ProdutoController implements IProdutosController {

	private static ProdutoController instance;
	private IRepositorioProduto repositorio;
	
	public static ProdutoController getInstance() {
		if (instance == null) {
			instance = new ProdutoController();
		}
		return instance;
	}
	
	@Override
	public void cadastrar(Produto produto) throws ProdutoException {
		if (produto == null) {
			throw new ProdutoException("Não há produto para inserir.");
		}
		if (repositorio.consultar(produto.getCodigo()) != null) {
			throw new ProdutoException("Produto já existe");
		}
		repositorio.cadastrar(produto);
	}

	@Override
	public void remover(int codigo) throws ProdutoException {
		instance.remover(codigo);
	}

	@Override
	public List<Produto> listarProdutos() throws ProdutoException {
		return instance.listarProdutos();
		
	}

	@Override
	public Produto consultar(int codigo) throws ProdutoException {
		for (Produto  p: repositorio.listar()) {
			if (codigo == p.getCodigo()) {
				return p;
			}
		}
		return null;
	}


}
