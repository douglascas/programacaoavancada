package controller.api;

import java.util.List;

import model.Fornecedor;
import repositorio.erros.FornecedorException;

public interface IFornecedorController {
	void cadastrarFornecedor(Fornecedor fornecedor) throws FornecedorException;
	Fornecedor consultarFornecedor(String cnpj) throws FornecedorException;
	void removerFornecedor(String cnpj) throws FornecedorException;
	List<Fornecedor> listarFornecedores() throws FornecedorException;
}
