package controller.api;

import java.util.List;

import model.Produto;
import repositorio.erros.ProdutoException;

public interface IProdutosController {
	void cadastrar(Produto p) throws ProdutoException;
	void remover (int codigo) throws ProdutoException;
	List<Produto> listarProdutos() throws ProdutoException;
	Produto consultar(int codigo) throws ProdutoException;
}
