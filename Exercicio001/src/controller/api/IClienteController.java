package controller.api;

import java.util.List;

import model.Cliente;
import repositorio.erros.ClienteException;

public interface IClienteController {
	void cadastrar(Cliente cliente) throws ClienteException;
	Cliente consultar(String cpf) throws ClienteException;
	void remover(String cpf) throws ClienteException;
	List<Cliente> listar() throws ClienteException;
}
