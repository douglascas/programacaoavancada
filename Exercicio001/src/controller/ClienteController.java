package controller;

import java.util.List;

import iRepositorio.IRepositorioCliente;
import model.Cliente;
import repositorio.erros.ClienteException;

public class ClienteController {

	private static ClienteController instance;
	private IRepositorioCliente repositorio;
	
	public static ClienteController getInstance() {
		if (instance == null) {
			instance = new ClienteController();
		}
		return instance;
	}
	
	public void cadastrar(Cliente cliente) throws ClienteException {
		if (cliente == null) {
			throw new ClienteException("Cliente nao existe");
		}
		if (repositorio.consultar(cliente.getCpf()) != null) {
			throw new ClienteException("Cliente ja existe");
		}
		repositorio.cadastrar(cliente);
	}

	public Cliente consultar(Cliente cliente) throws ClienteException {
		for (Cliente c: repositorio.listar()) {
			if (c.getId() == cliente.getId()) {
				return c;
			}
		}
		return null;
	}

	public void remover(Cliente cliente) throws ClienteException {
		repositorio.remover(cliente);
	}

	public List<Cliente> listar() throws ClienteException {
		return repositorio.listar();
		
	}

}
