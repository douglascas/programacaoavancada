package controller.fachada;

import java.util.List;

import controller.FornecedorController;
import controller.ProdutoController;
import controller.api.IClienteController;
import controller.api.IFornecedorController;
import controller.api.IProdutosController;
import model.Cliente;
import model.Fornecedor;
import model.Produto;
import repositorio.erros.ClienteException;
import repositorio.erros.FornecedorException;
import repositorio.erros.ProdutoException;

public class MercadoFachada implements IFornecedorController {

	private static MercadoFachada instance;
	
	private IFornecedorController fornecedorController;
	private IProdutosController produtoController;
	private IClienteController clienteController;

	public static MercadoFachada getInstance() {
		if (instance == null) {
			instance = new MercadoFachada();
		}
		return instance;
	}
	
	public MercadoFachada() {
		fornecedorController = FornecedorController.getInstance();
		produtoController = ProdutoController.getInstance();
	}

	/**
	 * Métodos para o FORNECEDOR
	 */
	
	public void cadastrarFornecedor(Fornecedor f) throws FornecedorException {
		this.fornecedorController.cadastrarFornecedor(f);
	}

	@Override
	public Fornecedor consultarFornecedor(String cnpj) throws FornecedorException {
		return fornecedorController.consultarFornecedor(cnpj);
	}

	@Override
	public void removerFornecedor(String cnpj) throws FornecedorException {
		fornecedorController.removerFornecedor(cnpj);
	}

	@Override
	public List<Fornecedor> listarFornecedores() throws FornecedorException {
		return fornecedorController.listarFornecedores();
	}

	/**
	 * Métodos para o PRODUTO
	 */

	public void cadastrarProduto(Produto produto) throws ProdutoException {
		this.produtoController.cadastrar(produto);
	}

	public Produto consultar(Produto produto) throws ProdutoException {
		return produtoController.consultar(produto.getCodigo());
	}

	public void remover(Produto produto) throws ProdutoException {
		produtoController.remover(produto.getCodigo());
	}

	public List<Produto> listarProdutos() throws ProdutoException {
		return produtoController.listarProdutos();
	}
	
	/**
	 * Métodos para o CLIENTE
	 */

	public void cadastrarCliente(Cliente cliente) throws ClienteException {
		this.clienteController.cadastrar(cliente);
	}

	public Cliente consultarCliente(String cpf) throws ClienteException {
		return clienteController.consultar(cpf);
	}

	public void removerCliente(String cpf) throws ClienteException {
		clienteController.remover(cpf);
	}

	public List<Cliente> listarClientes() throws ClienteException {
		return clienteController.listar();
	}
	
}
