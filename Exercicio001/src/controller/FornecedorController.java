package controller;

import java.util.List;

import controller.api.IFornecedorController;
import iRepositorio.IRepositorioFornecedor;
import model.Fornecedor;
import repositorio.FornecedorRepositorio;
import repositorio.erros.FornecedorException;

public class FornecedorController implements IFornecedorController {

	private static FornecedorController instance;
	
	public static FornecedorController getInstance() {
		if (instance == null) {
			instance = new FornecedorController();
		}
		return instance;
	}
	
	private IRepositorioFornecedor repositorio;
	
	public FornecedorController() {
		this.repositorio = FornecedorRepositorio.getInstance();
	}

	
	@Override
	public void cadastrarFornecedor(Fornecedor f) throws FornecedorException {
		if (f == null) {
			throw new FornecedorException("Fornecedor nao existe");
		}
		if (repositorio.consultarFornecedor(f.getCnpj()) != null) {
			throw new FornecedorException("Fornecedor ja existe");
		}
		repositorio.cadastrarFornecedor(f);
	}

	@Override
	public Fornecedor consultarFornecedor(String cnpj) throws FornecedorException {
		for (Fornecedor f: repositorio.listarFornecedor()) {
			if (cnpj == f.getCnpj()) {
				return f;
			}
		}
		return null;
	}

	@Override
	public void removerFornecedor(String cnpj) throws FornecedorException {
		repositorio.removerFornecedor(cnpj);
	}

	@Override
	public List<Fornecedor> listarFornecedores() throws FornecedorException {
		return repositorio.listarFornecedor();
		
	}

}
