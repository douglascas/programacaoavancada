package principal;

import controller.fachada.MercadoFachada;
import model.Cliente;
import model.Fornecedor;

public class Principal {

	public static void main(String[] args) throws Exception {

		MercadoFachada mercadoFachada = MercadoFachada.getInstance();
		
		mercadoFachada.cadastrarFornecedor(new Fornecedor("José", "Beco 1", "34384787","123456", "José Fornecedor","10/09/2019"));
		mercadoFachada.cadastrarFornecedor(new Fornecedor("Maria", "Avenida 1", "34384787","323", "Maria Fornecedor","10/09/2019"));
		mercadoFachada.cadastrarFornecedor(new Fornecedor("Severia", "Rua 1", "34384787","12345126", "Severina Fornecedor","10/09/2019"));
		
		mercadoFachada.listarFornecedores().forEach((fornecedor) -> mostrarFornecedor(fornecedor));
		
		System.out.println("Consulta 323");
		Fornecedor resultado = mercadoFachada.consultarFornecedor("323");
		System.out.println(resultado.getCnpj());
		
		mercadoFachada.removerFornecedor("323");
		mercadoFachada.listarFornecedores().forEach((fornecedor) -> mostrarFornecedor(fornecedor));
		
	}
	
	
	public static Cliente criarCliente() {
		Cliente c = new Cliente("Douglas", "as", "Avenida 1", "34384787", "1203912039", "cliente1", "12345");
		return c;
	}
	
	public static Fornecedor criarFornecedor(Fornecedor fornecedor) {
		Fornecedor f = new Fornecedor("José 123123 ", "Avenida 1", "34384787","123456", "José Fornecedor","10/09/2019");
		return f;
	}
	
	public static void mostrarFornecedor(Fornecedor f) {
		System.out.println(f.getId() + " - " + f.getNome());
	}
	
}

