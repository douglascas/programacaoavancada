package iRepositorio;

import java.util.List;
import model.Fornecedor;
import repositorio.erros.FornecedorException;

public interface IRepositorioFornecedor {
	public void cadastrarFornecedor(Fornecedor fornecedor) throws FornecedorException;
	public void removerFornecedor(String cnpj) throws FornecedorException;
	public Fornecedor consultarFornecedor(String cnpj) throws FornecedorException;
	public List<Fornecedor> listarFornecedor();
}
