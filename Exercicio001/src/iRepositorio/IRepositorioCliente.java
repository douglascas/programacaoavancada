package iRepositorio;

import java.util.List;
import model.Cliente;
import repositorio.erros.ClienteException;

public interface IRepositorioCliente {
	public void cadastrar(Cliente cliente) throws ClienteException;
	public void remover(Cliente cliente) throws ClienteException;
	public Cliente consultar(String cpf) throws ClienteException;
	public List<Cliente> listar();
}
