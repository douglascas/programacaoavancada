package iRepositorio;

import java.util.List;
import model.Produto;
import repositorio.erros.ProdutoException;

public interface IRepositorioProduto {
	public void cadastrar(Produto produto) throws ProdutoException;
	public void remover(Produto produto) throws ProdutoException;
	public Produto consultar(int id) throws ProdutoException;
	public List<Produto> listar();
}
