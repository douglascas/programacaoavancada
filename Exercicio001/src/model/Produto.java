package model;

public class Produto {
	private int codigo;
	private String nome;
	private String desc;
	private int qtdEstoque;
	private Fornecedor fornecedor;
	
	public Produto(int codigo, String nome, String desc, int qtdEstoque,Fornecedor fornecedor) {
		super();
		this.codigo = codigo;
		this.nome = nome;
		this.desc = desc;
		this.qtdEstoque = qtdEstoque;
		this.fornecedor = fornecedor;
	}
	
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public int getQtdEstoque() {
		return qtdEstoque;
	}
	public void setQtdEstoque(int qtdEstoque) {
		this.qtdEstoque = qtdEstoque;
	}
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	
	
}
