package model;

public class Cliente extends Entidade {
	private String cpf;
	private String dtNascimento;
	private String login;
	private String senha;
	
	public Cliente(String nome, String endereco, String telefone,String cpf, String dtNascimento, String login, String senha) {
		super(nome, endereco, telefone);
		this.cpf = cpf;
		this.dtNascimento = dtNascimento;
		this.login = login;
		this.senha = senha;
	}
	
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getDtNascimento() {
		return dtNascimento;
	}
	public void setDtNascimento(String dtNascimento) {
		this.dtNascimento = dtNascimento;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}

	
}
