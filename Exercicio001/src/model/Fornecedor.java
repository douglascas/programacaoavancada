package model;

public class Fornecedor extends Entidade {
	private String cnpj;
	private String nmFantasia;
	private String dtAbertura;
	
	public Fornecedor(String nome, String endereco, String telefone, String cnpj, String nmFantasia, String dtAbertura) {
		super(nome, endereco, telefone);
		this.cnpj = cnpj;
		this.nmFantasia = nmFantasia;
		this.dtAbertura = dtAbertura;
	}
	
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getNmFantasia() {
		return nmFantasia;
	}
	public void setNmFantasia(String nmFantasia) {
		this.nmFantasia = nmFantasia;
	}
	public String getDtAbertura() {
		return dtAbertura;
	}
	public void setDtAbertura(String dtAbertura) {
		this.dtAbertura = dtAbertura;
	}
	
	
}
